package com.setmobile.aifms.entities;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author john
 */
@Entity
@Table(name = "tenant")
public class TenantEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "tnt_id")
	private Integer tntId;
	@Column(name = "name")
	private String name;
	@Column(name = "contact_person")
	private String contactPerson;
	@Column(name = "email")
	private String email;
	@Column(name = "datain")
	private String datain;
	@Column(name = "dataout")
	private String dataout;
	@Column(name = "tenant_state")
	private String tenantState;
	@ManyToOne
	@JoinColumn(name = "instance_id")
	private InstanceEntity instanceId;
	@Column(name = "backoffice_ip")
	private String backofficeIp;
	@Column(name = "backoffice_port")
	private Integer backofficePort;
	@Column(name = "sync_server_ip")
	private String syncServerIp;
	@Column(name = "sync_server_port")
	private Integer syncServerPort;
	@Column(name = "phone")
	private String phone;
	@Column(name = "country")
	private String country;
	@Column(name = "verificationToken")
	private String verificationToken;
	//@OneToMany(cascade = CascadeType.ALL, mappedBy = "tenant")
	//List<MobileDeviceEntity> mobileDeviceEntities;
	
	public TenantEntity() {
	}

	public TenantEntity(Integer tntId) {
		this.tntId = tntId;
	}

	public Integer getTntId() {
		return tntId;
	}

	public void setTntId(Integer tntId) {
		this.tntId = tntId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDatain() {
		return datain;
	}

	public void setDatain(String datain) {
		this.datain = datain;
	}

	public String getDataout() {
		return dataout;
	}

	public void setDataout(String dataout) {
		this.dataout = dataout;
	}

	public String getTenantState() {
		return tenantState;
	}

	public void setTenantState(String tenantState) {
		this.tenantState = tenantState;
	}

	public InstanceEntity getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(InstanceEntity instanceId) {
		this.instanceId = instanceId;
	}

	public String getBackofficeIp() {
		return backofficeIp;
	}

	public void setBackofficeIp(String backofficeIp) {
		this.backofficeIp = backofficeIp;
	}

	public Integer getBackofficePort() {
		return backofficePort;
	}

	public void setBackofficePort(Integer backofficePort) {
		this.backofficePort = backofficePort;
	}

	public String getSyncServerIp() {
		return syncServerIp;
	}

	public void setSyncServerIp(String syncServerIp) {
		this.syncServerIp = syncServerIp;
	}

	public Integer getSyncServerPort() {
		return syncServerPort;
	}

	public void setSyncServerPort(Integer syncServerPort) {
		this.syncServerPort = syncServerPort;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getVerificationToken() {
		return verificationToken;
	}

	public void setVerificationToken(String verificationToken) {
		this.verificationToken = verificationToken;
	}
/*
	public List<MobileDeviceEntity> getMobileDeviceEntities() {
		return mobileDeviceEntities;
	}

	public void setMobileDeviceEntities(List<MobileDeviceEntity> mobileDeviceEntities) {
		this.mobileDeviceEntities = mobileDeviceEntities;
	}
*/
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (tntId != null ? tntId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof TenantEntity)) {
			return false;
		}
		TenantEntity other = (TenantEntity) object;
		if ((this.tntId == null && other.tntId != null) || (this.tntId != null && !this.tntId.equals(other.tntId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "entities.TenantEntity[ tntId=" + tntId + " ]";
	}

}
