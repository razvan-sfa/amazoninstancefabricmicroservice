package com.setmobile.aifms.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "instance")
public class InstanceEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ins_id")
	private Integer insId;
	@Column(unique = true, name = "instance_id")
	private String instanceId;
	@Column(name = "instance_name")
	private String instanceName;
	@Column(name = "instance_application")
	private String instanceApplication;
	@Column(name = "instance_type")
	private String instanceType;
	@Column(name = "image_id")
	private String imageId;
	@Column(name = "availability_zone")
	private String availabilityZone;
	@Column(name = "instance_state")
	private String instanceState;
	@Column(name = "storage")
	private String storage;
	@Column(name = "key_name")
	private String keyName;
	@Column(name = "security_group")
	private String securityGroup;
	@Column(name = "public_ip")
	private String publicIp;
	@Column(name = "private_ip")
	private String privateIp;
	@Column(name = "public_dns")
	private String publicDns;
	@Column(name = "private_dns")
	private String privateDns;
	// @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy =
	// "instanceId")
	// List<TenantEntity> tenantEntities;

	public InstanceEntity() {
	}

	public InstanceEntity(Integer insId) {
		this.insId = insId;
	}

	public Integer getInsId() {
		return insId;
	}

	public void setInsId(Integer insId) {
		this.insId = insId;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public String getInstanceApplication() {
		return instanceApplication;
	}

	public void setInstanceApplication(String instanceApplication) {
		this.instanceApplication = instanceApplication;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getAvailabilityZone() {
		return availabilityZone;
	}

	public void setAvailabilityZone(String availabilityZone) {
		this.availabilityZone = availabilityZone;
	}

	public String getInstanceState() {
		return instanceState;
	}

	public void setInstanceState(String instanceState) {
		this.instanceState = instanceState;
	}

	public String getStorage() {
		return storage;
	}

	public void setStorage(String storage) {
		this.storage = storage;
	}

	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public String getSecurityGroup() {
		return securityGroup;
	}

	public void setSecurityGroup(String securityGroup) {
		this.securityGroup = securityGroup;
	}

	public String getPublicIp() {
		return publicIp;
	}

	public void setPublicIp(String publicIp) {
		this.publicIp = publicIp;
	}

	public String getPrivateIp() {
		return privateIp;
	}

	public void setPrivateIp(String privateIp) {
		this.privateIp = privateIp;
	}

	public String getPublicDns() {
		return publicDns;
	}

	public void setPublicDns(String publicDns) {
		this.publicDns = publicDns;
	}

	public String getPrivateDns() {
		return privateDns;
	}

	public void setPrivateDns(String privateDns) {
		this.privateDns = privateDns;
	}

	/*
	 * public List<TenantEntity> getTenantEntities() { return tenantEntities; }
	 * 
	 * public void setTenantEntities(List<TenantEntity> tenantEntities) {
	 * this.tenantEntities = tenantEntities; }
	 */
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (insId != null ? insId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof InstanceEntity)) {
			return false;
		}
		InstanceEntity other = (InstanceEntity) object;
		if ((this.insId == null && other.insId != null) || (this.insId != null && !this.insId.equals(other.insId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "InstanceEntity [insId=" + insId + ", instanceId=" + instanceId + ", instanceName=" + instanceName
				+ ", instanceApplication=" + instanceApplication + ", instanceType=" + instanceType + ", imageId="
				+ imageId + ", availabilityZone=" + availabilityZone + ", instanceState=" + instanceState + ", storage="
				+ storage + ", keyName=" + keyName + ", securityGroup=" + securityGroup + ", publicIp=" + publicIp
				+ ", privateIp=" + privateIp + ", publicDns=" + publicDns + ", privateDns=" + privateDns + "]";
	}

}
