package com.setmobile.aifms.util;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;
import com.setmobile.aifms.entities.InstanceEntity;

public final class EC2ManagementConsole {
	private static Logger log = Logger.getLogger(EC2ManagementConsole.class);

	public static boolean isInstanceStarted(AmazonEC2 ec2, Instance instance) {
		try {
			DescribeInstancesRequest instanceRequest = new DescribeInstancesRequest();
			ArrayList<String> instanceLookup = new ArrayList<String>();
			instanceLookup.add(instance.getInstanceId());
			instanceRequest.setInstanceIds(instanceLookup);
			DescribeInstancesResult describeInstancesResult = ec2.describeInstances(instanceRequest);
			log.info("DescribeInstancesResult " + describeInstancesResult);
			if (describeInstancesResult.getReservations().get(0).getInstances().get(0).getState().getName()
					.equals("running")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	public static String getInstanceState(AmazonEC2 ec2, InstanceEntity instanceEntity){
		log.info("getInstanceState");
		DescribeInstancesRequest instanceRequest = new DescribeInstancesRequest();
		ArrayList<String> instanceLookup = new ArrayList<String>();
		instanceLookup.add(instanceEntity.getInstanceId());
		instanceRequest.setInstanceIds(instanceLookup);
		DescribeInstancesResult describeInstancesResult = ec2.describeInstances(instanceRequest);
		log.info("DescribeInstancesResult " + describeInstancesResult);
		return describeInstancesResult.getReservations().get(0).getInstances().get(0).getState().getName();
		
	}
	
	public static void startInstance(AmazonEC2 ec2, InstanceEntity instanceEntity){
		log.info("startInstance");
		StartInstancesRequest startRequest = new StartInstancesRequest().withInstanceIds(instanceEntity.getInstanceId());
	    StartInstancesResult startResult = ec2.startInstances(startRequest);
	    log.info("startResult " + startResult);
	}
	
	public static void tagInstance(Instance instance, String key, String value){
		
	}
	
}
