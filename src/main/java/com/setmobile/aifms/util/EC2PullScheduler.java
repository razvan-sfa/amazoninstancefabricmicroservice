package com.setmobile.aifms.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.neo4j.cypher.internal.compiler.v2_1.perty.docbuilders.toStringDocBuilder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.Tag;
import com.setmobile.aifms.entities.InstanceEntity;
import com.setmobile.aifms.model.AwsEc2Instance;
import com.setmobile.aifms.repository.InstanceRepository;


@Component
public class EC2PullScheduler {
	
	private static final Logger log = Logger.getLogger(EC2PullScheduler.class);
	
	@Autowired
	private InstanceRepository instanceRepository;
	
	@Autowired
	private AmazonEC2 ec2;
	
	@Autowired
	private Properties strings;
	
	@PostConstruct
	public void postConstruct(){
		syncronizeInstances();
	}
	
	//@Scheduled(fixedRate = 50000) //every 5 seconds
	//@Scheduled(cron="0 */5 * * * *") //every 5 minutes
	@Scheduled(cron="0 0 * * * *") //every hour
	public void scheduled(){
		syncronizeInstances();
	}
	private void syncronizeInstances(){
		log.info("syncronizeInstances()");
		
		//get instances from DB
		List<InstanceEntity> instanceEntitiesFromDB = instanceRepository.findAll();
		
		//get instances from Amazon
		List<InstanceEntity> instanceEntitiesFromCloud = new ArrayList<>();
		List<AwsEc2Instance> amazonCloudInstancesList = new ArrayList<>();
		DescribeInstancesRequest request = new DescribeInstancesRequest();
		DescribeInstancesResult result = ec2.describeInstances(request);
		List<Reservation> reservations = result.getReservations();
		for (Reservation reservation : reservations) {
			List<Instance> instances = reservation.getInstances();
			for (Instance instance : instances) {
				AwsEc2Instance amazonEc2Instance = new AwsEc2Instance();
				amazonEc2Instance.setInstanceId(instance.getInstanceId());
				amazonEc2Instance.setImageId(instance.getImageId());
				amazonEc2Instance.setInstanceState(instance.getState().getName());
				amazonEc2Instance.setKeyName(instance.getKeyName());
				amazonEc2Instance.setInstanceType(instance.getInstanceType());
				amazonEc2Instance.setAvailabilityZone(instance.getPlacement().getAvailabilityZone());
				amazonEc2Instance.setPublicIp(instance.getPublicIpAddress());
				amazonEc2Instance.setPrivateIp(instance.getPrivateIpAddress());
				amazonEc2Instance.setSecurityGroup(instance.getSecurityGroups().get(0).getGroupName());
				amazonEc2Instance.setPublicDns(instance.getPublicDnsName());
				amazonEc2Instance.setPrivateDns(instance.getPrivateDnsName());
				amazonEc2Instance.setStorage(instance.getBlockDeviceMappings().get(0).getEbs().getVolumeId());
				amazonCloudInstancesList.add(amazonEc2Instance);
				List<Tag> tags = instance.getTags(); 
				for (Tag t : tags){
					if (t.getKey().equals(strings.get("amazon.instance.tag.name").toString())){
						amazonEc2Instance.setInstanceName(t.getValue());
					} else {
						if (t.getKey().equals(strings.get("amazon.instance.tag.application").toString())){
							amazonEc2Instance.setInstanceApplication(t.getValue());
						}
					}
				}
				
				InstanceEntity instanceEntityFromCloud = new InstanceEntity();
				try {
					BeanUtils.copyProperties(amazonEc2Instance, instanceEntityFromCloud);
				} catch (Exception e) {
					log.info(e.getLocalizedMessage());
				}
				
				instanceEntitiesFromCloud.add(instanceEntityFromCloud);
			}
		}
		

		
		String [] ignoredProperties = {"insId"};
		for (InstanceEntity id : instanceEntitiesFromDB){
			Boolean exists = Boolean.FALSE;
			for (InstanceEntity ic : instanceEntitiesFromCloud) {
				//update existing instances
				if (id.getInstanceId().equals(ic.getInstanceId())) {
					exists = Boolean.TRUE;
					BeanUtils.copyProperties(ic, id, ignoredProperties);
					log.info("Existing instance.");
					log.info("Update instance:" + id.toString());
					instanceRepository.save(id);
					//instanceRepository.
				}
			}
			//delete terminated instances			
			if (!exists){
				log.info("Instance terminated!");
				log.info("deleting instance: " + id.toString());
				instanceRepository.delete(id);
			}
		}
		
		//insert new instances
		for (InstanceEntity ic : instanceEntitiesFromCloud) {
			Boolean exists = Boolean.FALSE;
			for (InstanceEntity id : instanceEntitiesFromDB){
				if (ic.getInstanceId().equals(id.getInstanceId())) {
					exists = Boolean.TRUE;
					break;
				}
			}
			if (!exists){
				log.info("New instance found");
				log.info("Inserting instance:" + ic.toString());
				instanceRepository.save(ic);
			}
			
		}
		
	}
	
	
}
