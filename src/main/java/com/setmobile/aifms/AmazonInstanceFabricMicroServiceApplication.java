package com.setmobile.aifms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.spring.JaxRsConfig;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.setmobile.aifms.config.CXFWebServicesConfig;
import com.setmobile.aifms.web.rest.AmazonInstanceWebService;

@SpringBootApplication
@EnableScheduling
//@ComponentScan("com.setmobile.aifms.web.rest")
@Import({ JaxRsConfig.class, CXFWebServicesConfig.class })
public class AmazonInstanceFabricMicroServiceApplication {

	@Value("${amazon.region}")
	private String amazonRegion;
	
	public static void main(String[] args) {
		SpringApplication.run(AmazonInstanceFabricMicroServiceApplication.class, args);
	}

	@Bean
	public ServletRegistrationBean servletRegistrationBean(ApplicationContext context) {
		return new ServletRegistrationBean(new CXFServlet(), "/services/*");
	}

	@Bean
	public Properties strings() throws IOException{
		Resource resource = new ClassPathResource("strings.properties");
		Properties props = PropertiesLoaderUtils.loadProperties(resource);	
		return props;
	}
	
	@Bean
	public AmazonEC2 amazonEC2(){
		AmazonEC2 ec2;
		AWSCredentials credentials = null;
		try {
			credentials = new ProfileCredentialsProvider().getCredentials();
		} catch (Exception e) {
			throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
					+ "Please make sure that your credentials file is at the correct "
					+ "location (~/.aws/credentials), and is in valid format.", e);
		}

		ec2 = new AmazonEC2Client(credentials);
		Region region = Region.getRegion(Regions.fromName(amazonRegion));
		ec2.setEndpoint(region.getServiceEndpoint("ec2"));
		return ec2;
	}
	
	@Bean
	@Autowired
	public Server rsServer(AmazonInstanceWebService amazonInstanceWebService) {
		JAXRSServerFactoryBean endpoint = new JAXRSServerFactoryBean();
		endpoint.setServiceBean(amazonInstanceWebService);
		endpoint.setAddress("/instances");
		JacksonJsonProvider provider = new JacksonJsonProvider();
		List<Object> providers = new ArrayList<Object>();
		providers.add(provider);
		endpoint.setProviders(providers);
		return endpoint.create();
	}
	
}
