package com.setmobile.aifms.web.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.neo4j.cypher.internal.compiler.v2_2.ast.rewriters.getDegreeOptimizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AllocateAddressResult;
import com.amazonaws.services.ec2.model.AssociateAddressRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.Tag;
import com.setmobile.aifms.entities.InstanceEntity;
import com.setmobile.aifms.model.AwsEc2Instance;
import com.setmobile.aifms.repository.InstanceRepository;
import com.setmobile.aifms.repository.TenantRepository;
import com.setmobile.aifms.util.EC2ManagementConsole;
import com.setmobile.aifms.util.EC2PullScheduler;

@Component
@Path("/amazonInstance")
public class AmazonInstanceWebService {

	private static final Logger log = Logger.getLogger(AmazonInstanceWebService.class);

	@Value("${amazon.ami.image.id}")
	private String customAmiImageId;

	@Value("${amazon.instance.type}")
	private String customInstanceType;

	@Value("${amazon.key.name}")
	private String customKeyName;

	@Value("${amazon.security.group}")
	private String customSecurityGroup;

	//@Value("${amazon.region}")
	//private String amazonRegion;
	
	@Value("${amazon.instances.threshold}")
	Integer multitenaceToInstanceThreshold;

	@Autowired
	private Properties strings;
	
	@Autowired
	private AmazonEC2 ec2;
	
	@Autowired
	EC2PullScheduler ec2PullScheduler;
	
	private InstanceRepository instanceRepository; 
	private TenantRepository tenantRepository;
	
	

	
	@Autowired
	public AmazonInstanceWebService(InstanceRepository instanceRepository, TenantRepository tenantRepository) {
		this.instanceRepository = instanceRepository;
		this.tenantRepository = tenantRepository;
		
	}
	
	/*@PostConstruct
	private void init() {
		AWSCredentials credentials = null;
		try {
			credentials = new ProfileCredentialsProvider().getCredentials();
		} catch (Exception e) {
			throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
					+ "Please make sure that your credentials file is at the correct "
					+ "location (~/.aws/credentials), and is in valid format.", e);
		}

		ec2 = new AmazonEC2Client(credentials);
		Region region = Region.getRegion(Regions.fromName(amazonRegion));
		ec2.setEndpoint(region.getServiceEndpoint("ec2"));
	}
	*/
	
	@GET
	@Path("/getSomething")
	
	public String getSomething(){
		log.info(strings.get("tomcat"));
		log.info(strings.get("mysql"));
		ec2PullScheduler.scheduled();
		return "4";
	}
	
	
	@GET
	@Path("/getAvailableInstance")
	@Produces(MediaType.TEXT_PLAIN)
	public Integer getAvailableInstance()  {
		List<InstanceEntity> allInstances = instanceRepository.findAll();
		
		for (InstanceEntity instance: allInstances){
			log.info(instance.toString()  + " has " + tenantRepository.countByInstanceId(instance) + " tenants.");
			if (tenantRepository.countByInstanceId(instance) < multitenaceToInstanceThreshold) {
				log.info("Found available instance: " + instance.toString());
				//check if instance is started
				if (EC2ManagementConsole.getInstanceState(ec2, instance).equalsIgnoreCase("stopped")){
					EC2ManagementConsole.startInstance(ec2, instance);
				}
				return instance.getInsId();
			}
		}
		log.info("No available instance found!");
		return null;
	}
	
	@GET
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public InstanceEntity createInstance() throws InterruptedException {
		int maxRetries = 10;
		int retries = 0;
		boolean instanceStarted = false;
		InstanceEntity instance = new InstanceEntity();
		instance.setInstanceType(customInstanceType);
		instance.setKeyName(customKeyName);
		instance.setSecurityGroup(customSecurityGroup);
		//instance.setRegion(amazonRegion); incerc fara sa ii setez regiunea
		instance.setInstanceState("pending");

		RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
		runInstancesRequest.withImageId(customAmiImageId).withInstanceType(customInstanceType).withMinCount(1)
				.withMaxCount(1).withKeyName(customKeyName).withSecurityGroupIds(customSecurityGroup);
		RunInstancesResult runInstancesResult = ec2.runInstances(runInstancesRequest);
		log.info("RunInstancesResult " + runInstancesResult.toString());

		Instance createdInstance = runInstancesResult.getReservation().getInstances().get(0);
		log.info("CreatedInstance " + createdInstance.toString());
		instance.setInstanceId(createdInstance.getInstanceId());
		instance.setImageId(createdInstance.getImageId());

		// wait for instance start
		while ((!instanceStarted) && (retries < maxRetries)) {
			Thread.sleep(10000);
			if (EC2ManagementConsole.isInstanceStarted(ec2, createdInstance)) {
				log.info("Instance has been successfully created!");
				instanceStarted = true;
				instance.setInstanceState("running");
				
				//create new Elastic IP
				AllocateAddressResult allocateAddressResult = ec2.allocateAddress();
				log.info("AllocateAddressResult " + allocateAddressResult.toString());
				
				//associate the Elastic IP with the instance
				AssociateAddressRequest associateAddressRequest = new AssociateAddressRequest();
				associateAddressRequest.setInstanceId(createdInstance.getInstanceId());
				associateAddressRequest.setPublicIp(allocateAddressResult.getPublicIp());
				ec2.associateAddress(associateAddressRequest);
				instance.setPublicIp(allocateAddressResult.getPublicIp());
				
			} else {
				retries++;
				log.info("Wait for instance " + createdInstance.getInstanceId() + " to start, retry " + retries + ".");
			}
		}

		instanceRepository.save(instance);
		
		if (!instanceStarted) {
			throw new RuntimeException("Timeout exceeded. An error occured while creating the instance.");

		}
		
		return instance;
	}

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AwsEc2Instance> listInstances() {
		log.info("listInstances()");
		List<AwsEc2Instance> amazonCloudInstancesList = new ArrayList<>();
		DescribeInstancesRequest request = new DescribeInstancesRequest();
		DescribeInstancesResult result = ec2.describeInstances(request);
		List<Reservation> reservations = result.getReservations();
		for (Reservation reservation : reservations) {
			List<Instance> instances = reservation.getInstances();
			for (Instance instance : instances) {
				AwsEc2Instance amazonEc2Instance = new AwsEc2Instance();
				amazonEc2Instance.setInstanceId(instance.getInstanceId());
				amazonEc2Instance.setImageId(instance.getImageId());
				amazonEc2Instance.setInstanceState(instance.getState().getName());
				amazonEc2Instance.setKeyName(instance.getKeyName());
				amazonEc2Instance.setInstanceType(instance.getInstanceType());
				amazonEc2Instance.setAvailabilityZone(instance.getPlacement().getAvailabilityZone());
				amazonEc2Instance.setPublicIp(instance.getPublicIpAddress());
				amazonEc2Instance.setPrivateIp(instance.getPrivateIpAddress());
				amazonEc2Instance.setSecurityGroup(instance.getSecurityGroups().get(0).getGroupName());
				amazonEc2Instance.setPublicDns(instance.getPublicDnsName());
				amazonEc2Instance.setPrivateDns(instance.getPrivateDnsName());
				amazonEc2Instance.setStorage(instance.getBlockDeviceMappings().get(0).getEbs().getVolumeId());
				amazonCloudInstancesList.add(amazonEc2Instance);
				List<Tag> tags = instance.getTags(); 
				for (Tag t : tags){
					if (t.getKey().equals(strings.get("amazon.instance.tag.name"))){
						amazonEc2Instance.setInstanceName(t.getValue());
					} else {
						if (t.getKey().equals(strings.get("amazon.instance.tag.application"))){
							amazonEc2Instance.setInstanceApplication(t.getValue());
						}
					}
				}
				
			}
		}
		return amazonCloudInstancesList;
	}
}
