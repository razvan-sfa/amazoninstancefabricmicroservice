package com.setmobile.aifms.model;

public class AwsEc2Instance {

	private String instanceId;
	private String instanceName;
	private String instanceApplication;
	private String instanceType;
	private String imageId;
	private String availabilityZone;
	private String instanceState;
	private String keyName;
	private String securityGroup;
	private String publicIp;
	private String privateIp;
	private String publicDns;
	private String privateDns;
	private String storage;

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public String getSecurityGroup() {
		return securityGroup;
	}

	public void setSecurityGroup(String securityGroup) {
		this.securityGroup = securityGroup;
	}

	public String getAvailabilityZone() {
		return availabilityZone;
	}

	public void setAvailabilityZone(String availabilityZone) {
		this.availabilityZone = availabilityZone;
	}

	public String getPublicIp() {
		return publicIp;
	}

	public void setPublicIp(String publicIp) {
		this.publicIp = publicIp;
	}

	public String getInstanceState() {
		return instanceState;
	}

	public void setInstanceState(String instanceState) {
		this.instanceState = instanceState;
	}

	public String getPrivateIp() {
		return privateIp;
	}

	public void setPrivateIp(String privateIp) {
		this.privateIp = privateIp;
	}

	public String getPublicDns() {
		return publicDns;
	}

	public void setPublicDns(String publicDns) {
		this.publicDns = publicDns;
	}

	public String getPrivateDns() {
		return privateDns;
	}

	public void setPrivateDns(String privateDns) {
		this.privateDns = privateDns;
	}

	public String getStorage() {
		return storage;
	}

	public void setStorage(String storage) {
		this.storage = storage;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public String getInstanceApplication() {
		return instanceApplication;
	}

	public void setInstanceApplication(String instanceApplication) {
		this.instanceApplication = instanceApplication;
	}

	@Override
	public String toString() {
		return "AwsEc2Instance [instanceId=" + instanceId + ", instanceName=" + instanceName + ", instanceApplication="
				+ instanceApplication + ", instanceType=" + instanceType + ", imageId=" + imageId
				+ ", availabilityZone=" + availabilityZone + ", instanceState=" + instanceState + ", keyName=" + keyName
				+ ", securityGroup=" + securityGroup + ", publicIp=" + publicIp + ", privateIp=" + privateIp
				+ ", publicDns=" + publicDns + ", privateDns=" + privateDns + ", storage=" + storage + "]";
	}

}
