package com.setmobile.aifms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.setmobile.aifms.entities.InstanceEntity;

public interface InstanceRepository extends JpaRepository<InstanceEntity, Integer>{

}
