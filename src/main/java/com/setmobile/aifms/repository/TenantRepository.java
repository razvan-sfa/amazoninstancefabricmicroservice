package com.setmobile.aifms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.setmobile.aifms.entities.InstanceEntity;
import com.setmobile.aifms.entities.TenantEntity;

public interface TenantRepository extends JpaRepository<TenantEntity, Integer> {
	int countByInstanceId(InstanceEntity instanceId);
}
