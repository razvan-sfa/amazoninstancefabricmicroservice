package com.bojo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

import com.setmobile.aifms.AmazonInstanceFabricMicroServiceApplication;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AmazonInstanceFabricMicroServiceApplication.class)
@WebAppConfiguration
public class AmazonInstanceFabricMicroServiceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
